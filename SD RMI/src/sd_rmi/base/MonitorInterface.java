package sd_rmi.base;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MonitorInterface extends Remote {
	String obterInfo () throws RemoteException;
	String monitorCpus () throws RemoteException;
	String sistemInfo () throws RemoteException;
	String memoInfo() throws RemoteException;
	String cpuInfo() throws RemoteException;
}