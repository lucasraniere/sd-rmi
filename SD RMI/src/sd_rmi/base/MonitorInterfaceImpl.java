package sd_rmi.base;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import org.hyperic.sigar.SigarException;

import sd_rmi.servidor.MonitorSistemaServidor;

public class MonitorInterfaceImpl extends UnicastRemoteObject implements MonitorInterface {
	
	private static final long serialVersionUID = -2818167030748183820L;
	
	public MonitorInterfaceImpl() throws RemoteException {
		super();
	}

	@Override
	public String obterInfo() {
		String info = "";
		try {
			info = new MonitorSistemaServidor().toStringClient();
		} catch (SigarException e) {
			e.printStackTrace();
		}
		return info;
	}
	
	public String monitorCpus() {
		String info = "";
		try {
			info = new MonitorSistemaServidor().monitorCpus();
		} catch (SigarException e) {
			e.printStackTrace();
		}
		return info;
	}
	
	public String sistemInfo() {
		String info = "";
		try {
			info = new MonitorSistemaServidor().sistemInfo();
		} catch (SigarException e) {
			e.printStackTrace();
		}
		return info;
	}
	
	public String memoInfo() {
		String info ="";
		try {
			info = new MonitorSistemaServidor().memoInfo();
		} catch (SigarException e) {
			e.printStackTrace();
		}
		return info;
	}
	
	public String cpuInfo() {
		String info = "";
		try {
			info = new MonitorSistemaServidor().cpuInfo();
		} catch (SigarException e) {
			e.printStackTrace();
		}
		return info;
	}
}
