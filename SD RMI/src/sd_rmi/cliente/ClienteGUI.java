package sd_rmi.cliente;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextPane;

import org.hyperic.sigar.SigarException;

import sd_rmi.base.MonitorInterface;

public class ClienteGUI {

	private JFrame frmMonitorDeSistema;
	
	// troque o valor de IP pelo ip do servidor
	private static final String IP = "172.17.60.185";
	
	
	JTextPane textPaneCpu2 = new JTextPane();
	JTextPane textPaneSistem = new JTextPane();
	JTextPane textPaneMemo = new JTextPane();
	JTextPane textPaneCpu = new JTextPane();

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClienteGUI window = new ClienteGUI();
					window.frmMonitorDeSistema.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws SigarException 
	 */
	public ClienteGUI() throws SigarException, RemoteException {
		initialize();
	}
	
	private void initialize() throws SigarException {
		
		

		textPaneCpu2.setEditable(false);
		textPaneCpu2.setBounds(369, 357, 374, 109);
		textPaneSistem.setEditable(false);
		textPaneSistem.setBounds(22, 43, 335, 109);
		textPaneMemo.setEditable(false);
		textPaneMemo.setBounds(22, 201, 335, 109);
		textPaneCpu.setEditable(false);
		textPaneCpu.setBounds(22, 357, 335, 109);
		
		JLabel labelMemo = new JLabel("Informações da Memória RAM");
		labelMemo.setBounds(12, 174, 238, 15);
		JLabel labelCpu = new JLabel("Informações do Processador");
		labelCpu.setBounds(12, 330, 238, 15);
		
		frmMonitorDeSistema = new JFrame();
		frmMonitorDeSistema.setTitle("Monitor de Sistema Cliente");
		frmMonitorDeSistema.setBounds(100, 100, 800, 600);
		frmMonitorDeSistema.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMonitorDeSistema.getContentPane().setLayout(null);
		
		JLabel labelSistem = new JLabel("Informações do Sistema");
		labelSistem.setBounds(12, 12, 216, 32);
		frmMonitorDeSistema.getContentPane().add(labelSistem);
		
		
		
		JButton buttonPrint = new JButton("Monitorar");
		buttonPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Registry r;
				try {
					System.setSecurityManager(new SecurityManager());
					
					r = LocateRegistry.getRegistry(IP, 1098);
					MonitorInterface monitor = (MonitorInterface) r.lookup("rmi://" + IP + "/monitor");
					
					Thread t = new Thread() {
						public void run() {
							while(true) {
								try {
									textPaneMemo.setText(monitor.memoInfo());
									textPaneCpu2.setText(monitor.monitorCpus());
									Thread.sleep(1000);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					};
					
					t.start();
					textPaneSistem.setText(monitor.sistemInfo());
					textPaneCpu.setText(monitor.cpuInfo());
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				

			}
		});
		buttonPrint.setBounds(501, 195, 157, 68);
		frmMonitorDeSistema.getContentPane().add(buttonPrint);
		frmMonitorDeSistema.getContentPane().add(textPaneSistem);
		frmMonitorDeSistema.getContentPane().add(labelMemo);
		frmMonitorDeSistema.getContentPane().add(textPaneMemo);
		frmMonitorDeSistema.getContentPane().add(labelCpu);
		frmMonitorDeSistema.getContentPane().add(textPaneCpu);
		frmMonitorDeSistema.getContentPane().add(textPaneCpu2);
	}
	

	


}
