package sd_rmi.servidor;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextPane;

import org.hyperic.sigar.SigarException;

import sd_rmi.base.MonitorInterface;
import sd_rmi.base.MonitorInterfaceImpl;

public class ServidorGUI {

	private JFrame frmMonitorDeSistema;
	
	// troque o valor do IP pelo ip da maquina onde o servidor ira rodar
	private static final String IP = "172.17.60.185";
	
	MonitorSistemaServidor info = new MonitorSistemaServidor();
	
	JTextPane textPaneCpu2 = new JTextPane();
	JTextPane textPaneSistem = new JTextPane();
	JTextPane textPaneMemo = new JTextPane();
	JTextPane textPaneCpu = new JTextPane();

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ServidorGUI window = new ServidorGUI();
					window.frmMonitorDeSistema.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws SigarException 
	 */
	public ServidorGUI() throws SigarException, RemoteException {
		servidorRMI();
		initialize();
	}
	
	public void servidorRMI() {
		try {
			System.setSecurityManager(new SecurityManager());
			
			Registry r = LocateRegistry.createRegistry(1098);
			System.setProperty("java.rmi.server.hostname", IP);
			MonitorInterface monitor = new MonitorInterfaceImpl();
			r.bind("rmi://" + IP + "/monitor", monitor);
			
			System.out.println("Servidor OK...");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SigarException 
	 */
	private void initialize() throws SigarException {
		
		

		textPaneCpu2.setEditable(false);
		textPaneCpu2.setBounds(369, 357, 374, 109);
		textPaneSistem.setEditable(false);
		textPaneSistem.setBounds(22, 43, 335, 109);
		textPaneMemo.setEditable(false);
		textPaneMemo.setBounds(22, 201, 335, 109);
		textPaneCpu.setEditable(false);
		textPaneCpu.setBounds(22, 357, 335, 109);
		
		JLabel labelMemo = new JLabel("Informações da Memória RAM");
		labelMemo.setBounds(12, 174, 238, 15);
		JLabel labelCpu = new JLabel("Informações do Processador");
		labelCpu.setBounds(12, 330, 238, 15);
		
		frmMonitorDeSistema = new JFrame();
		frmMonitorDeSistema.setTitle("Monitor de Sistema Servidor");
		frmMonitorDeSistema.setBounds(100, 100, 800, 600);
		frmMonitorDeSistema.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMonitorDeSistema.getContentPane().setLayout(null);
		
		JLabel labelSistem = new JLabel("Informações do Sistema");
		labelSistem.setBounds(12, 12, 216, 32);
		frmMonitorDeSistema.getContentPane().add(labelSistem);
		
		
		
		JButton buttonPrint = new JButton("Monitorar");
		buttonPrint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				infoEstat();
				infoDinami();
			}
		});
		buttonPrint.setBounds(501, 195, 157, 68);
		frmMonitorDeSistema.getContentPane().add(buttonPrint);
		frmMonitorDeSistema.getContentPane().add(textPaneSistem);
		frmMonitorDeSistema.getContentPane().add(labelMemo);
		frmMonitorDeSistema.getContentPane().add(textPaneMemo);
		frmMonitorDeSistema.getContentPane().add(labelCpu);
		frmMonitorDeSistema.getContentPane().add(textPaneCpu);
		frmMonitorDeSistema.getContentPane().add(textPaneCpu2);
	}
	
	public void infoEstat() {
		textPaneSistem.setText(info.sistemInfo());
		textPaneCpu.setText(info.cpuInfo());
	}
	
	public void infoDinami() {
		Thread t = new Thread() {
			public void run() {
				while (true) {
					try {
						textPaneMemo.setText(info.memoInfo());
						textPaneCpu2.setText(info.monitorCpus());
						Thread.sleep(1000);
					} catch (SigarException e) {
						e.printStackTrace();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		t.start();
	}


}
