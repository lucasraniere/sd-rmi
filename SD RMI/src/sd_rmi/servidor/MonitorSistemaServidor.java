package sd_rmi.servidor;

import java.text.DecimalFormat;

import org.hyperic.sigar.CpuInfo;
import org.hyperic.sigar.CpuPerc;
import org.hyperic.sigar.Mem;
import org.hyperic.sigar.Sigar;
import org.hyperic.sigar.SigarException;

public class MonitorSistemaServidor {
	
	private static Sigar sigar = new Sigar();
	Mem mem = sigar.getMem();

	CpuInfo[] infos = MonitorSistemaServidor.sigar.getCpuInfoList();
    CpuInfo info = infos[0];
    
    DecimalFormat df = new DecimalFormat("0.0000");
	
	private String sistemaOp;
	private String userNome;
	private String arqSistem;
	private String versSistem;
	private long memoTotal;
	private int cacheSize;
	private String fabricantCpu;
	private String modeloCpu;
	private float freq;
	private int totalCpu;
	
	public MonitorSistemaServidor() throws SigarException {
		
		this.sistemaOp = System.getProperty("os.name");
		this.userNome = System.getProperty("user.name");
		this.arqSistem = System.getProperty("os.arch");
		this.versSistem = System.getProperty("os.version");
		this.memoTotal = mem.getRam();
		this.cacheSize = (int) info.getCacheSize()/1024;
		this.fabricantCpu = info.getVendor();
		this.modeloCpu = info.getModel();
		this.freq = (float) info.getMhz()/1024;
		this.totalCpu = info.getTotalCores();
	}
	
	public String toStringClient() throws SigarException {
		mem = sigar.getMem();
		return (
				"Informações do Sistema:\n  Sistema Operacional: " + this.sistemaOp + "\n  Nome do Usuário: " + this.userNome
				+ "\n  Arquitetura do Sistema: " + this.arqSistem + "\n  Versão do Sistema: " + this.versSistem
				+ "\n\nInformações da Memória RAM:\n  Memória RAM Total: " + this.memoTotal + " MB" + "\n  Memória RAM Livre: " + mem.getActualFree()/1024/1024 + " MB"
				+ "\n  Memória RAM Usada: " + mem.getActualUsed()/1024/1024 + " MB" + "\n  Porcentagem de Memória Usada: " + df.format(mem.getUsedPercent()) + " %"
				+ "\n  Porcentagem de Memória Livre: " + df.format(mem.getFreePercent()) + " %" + "\n\nInformações do Processador: " 
				+ "\n  Fabricante do Processador: " + this.fabricantCpu + "\n  Modelo do Processador: " + this.modeloCpu
				+ "\n  Frequência: " + this.freq + " GHz" + "\n  Tamanho do Cache: " + this.cacheSize + " MB" + "\n  Quantidade de Núcleos: " + this.totalCpu);
	}
	
	public String sistemInfo() {
		return ("Sistema Operacional: " + this.sistemaOp + "\nNome do Usuário: " + this.userNome
				+ "\nArquitetura do Sistema: " + this.arqSistem + "\nVersão do Sistema: " + this.versSistem );
	}
	
	public String memoInfo() throws SigarException {
		mem = sigar.getMem();
		return ("Memória RAM Total: " + this.memoTotal + " MB" + "\nMemória RAM Livre: " + mem.getActualFree()/1024/1024 + " MB"
				+ "\nMemória RAM Usada: " + mem.getActualUsed()/1024/1024 + " MB" + "\nPorcentagem de Memória Usada: " + df.format(mem.getUsedPercent()) + " %"
				+ "\nPorcentagem de Memória Livre: " + df.format(mem.getFreePercent()) + " %");
	}
	
	public String cpuInfo() {
		return ("Fabricante do Processador: " + this.fabricantCpu + "\nModelo do Processador: " + this.modeloCpu
				+ "\nFrequência: " + this.freq + " GHz" + "\nTamanho do Cache: " + this.cacheSize + " MB" + "\nQuantidade de Núcleos: " + this.totalCpu);
	}
	
	public String monitorCpus() throws SigarException {
		CpuPerc[] cpus = sigar.getCpuPercList();
		String infoCpu = "";
        for (int i = 0; i < cpus.length; i++) {
        	infoCpu += "\nCPU: " + i + " - Uso da CPU: " + (String) df.format(cpus[i].getUser()*100) + "%";
        } 
        return infoCpu;
    }
	
	public String getUserNome() {
		return this.userNome;
	}
}
